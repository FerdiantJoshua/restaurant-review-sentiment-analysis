# Restaurant Review Sentiment Analysis
NLP AI Engineer Internship 2020 Task  
Ferdiant Joshua Muis  
Repo: https://gitlab.com/FerdiantJoshua/restaurant-review-sentiment-analysis/

## Task

### Background
Prosa is one of the leading artificial intelligence (AI) companies in indonesia. One of its focuses is to provide solutions to problems related to natural language processing or NLP. To solve those problems, AI engineers in Prosa mostly use deep learning methods. Therefore, familiarity with some deep learning methods is important for AI engineers in Prosa. 

The purpose of this assignment is to explore AI engineer candidates about :
1. Their familiarity with deep learning methods
2. Their thinking processes when solving an NLP problem
3. Their way of implementing solutions by means of their codes
4. Their analytical thinking when analyzing the AI models that they have developed by means of experiments. 

Thus, considering the above objectives, the performances of AI models, i.e their accuracy, will not be considered as an assessment component in this selection. 

### Instruction
Create an AI model that solves a problem described in the following section. The AI model must be based on deep learning methods. There is no restriction for the type of methods to be used. When implementing the AI model, Gitlab (gitlab.com) must be used. In case you do not have an account in it, please create one. For the demonstration, please create a function that loads your AI model and performs the task as well. 

Understanding customer voices is very important for a company to improve the quality of products and services. One of the ways is by identifying the sentiments of customer voices. Thus, the company could know whether its products or services are viewed positively or negatively.

Motivated by the above background, Prosa.ai will create an API service for sentiment analysis. Thus, Prosa.ai must develop an AI model that predicts the sentiment of a text whether it is positive or negative. Because deep learning methods achieved better performance in general, Prosa.ai decided to develop the AI model by using one of deep learning methods.

## Documentation

### Architecture
![Architecture_LSTM](img/architecture.png)

### Result
0 means negative sentiment  
1 means positive sentiment  
![Result](img/classification_report.png)

### Flow
1. Dataset analysis  
Recognizing dataset: frequency distribution, category distribution, length summary of the reviews.
2. Cleaning and normalization  
Prepare dataset for processing: lowercase, punctuations removal, numbers removal, words normalization ('sy' to 'saya', etc), stopwords removal
3. Tensor preparation  
Prepare tensor for training: tokenize, limit some variables (VOCAB_SIZE, MAX_SEQUENCE_LENGTH, EMBEDDING_DIMENSION), pad sequences, shuffle tensor.
4. Creating model and training  
Train the model using architecture mentioned before. Use sigmoid act_func as categorizer with _1e-3_ LR adam optimizer on binary_crossentropy loss function. Train-validation pair is set to 9:1.
5. Evaluation  
Evaluate the model: observing change of loss (determine underfitting/overfitting), print accuracy, precision, recall, f1 metrics.
6. Some prediction outputs for a little human proofing

### Additional
I tried several things during my experiment:
1. Adding and removing stopword "kurang" and "lebih". This two stopwords affect accuracy, and training very much. Non-removal of stopword "lebih" will slow down the training unintentionally.
2. As the dataset is imbalanced (negative:positive = 1:3), F1 score for the negative class is pretty low.
3. Experimenting VOCAB_SIZE and MAX_SEQUENCE_LENGTH. The first values are intuition-based numbers around the max VOCAB_SIZE and MAX_SEQUENCE_LENGTH. The second values are based on both of the max value. However the first outperforms the second on **avg_f1** by **7%**
4. The model is obviously on ovefitting condition, but it has quite high performance (based on accuracy and F1 metrics)